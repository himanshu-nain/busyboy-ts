import * as express from "express";
import * as busyboy from "connect-busboy";
import { createWriteStream,  } from "fs";
import { join } from "path";

class Application{

    private PORT: number;
    private app: express.Application;
    private uploadDir: string;

    constructor(){

        this.uploadDir = './uploads';
        this.PORT = Number.parseInt(process.env.PORT || '3000');
        this.app = express();
        this.app.use(
            busyboy(
                {
                    highWaterMark: 2*1024*1024,
                    limits: {fileSize: 50*1024*1024}
                }
            )
        );
        this.initRoutes();

    }

    private initRoutes(){

        this.app.route('/upload').post((req : express.Request, res: express.Response, next: express.NextFunction)=>{

            req.pipe(req.busboy);

            req.busboy.on(
                'file',
                (fieldname, file, filename)=>{

                    const fstream = createWriteStream(join(this.uploadDir, filename));
                    file.pipe(fstream);
                    fstream.on('close', ()=>{
                        res.send({success: true, message:"Upload complete"});
                    });

                }
            );

        });

        this.app.route('/').get((req : express.Request, res: express.Response) => {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<form action="upload" method="post" enctype="multipart/form-data">');
            res.write('<input type="file" name="fileToUpload"><br>');
            res.write('<input type="submit">');
            res.write('</form>');
            return res.end();
        });

    }

    start(){
        this.app.listen(this.PORT, ()=>{console.log("Running")});
    }

};

const App = new Application();
App.start();